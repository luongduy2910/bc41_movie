import React from 'react'
import { Desktop, Mobile, Tablet } from '../../Layout/Responsive'
import HeaderDestop from './HeaderDestop'
import HeaderTablet from './HeaderTablet'
import HeaderMobile from './HeaderMobile'

function Header() {
  return (
    <div>
      <Desktop>
        <HeaderDestop/>
      </Desktop>
      <Tablet>
        <HeaderTablet/>
      </Tablet>
      <Mobile>
        <HeaderMobile/>
      </Mobile>
    </div>
  )
}

export default Header