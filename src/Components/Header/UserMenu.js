import React from 'react'
import { localUserServ } from '../../service/localService'
import { useSelector } from 'react-redux'
import { NavLink } from 'react-router-dom';

function UserMenu() {
    let userInfo = useSelector(state => {
        return state.userReducer.userInfo ; 
    })
    let handleLogout = () => {
        localUserServ.remove() ; 
        window.location.href = "/" ; 
        // window.location.reload() ; 
    }
    let renderContent = () => {
        if (userInfo) {
            return (
                <>
                    <span>{userInfo.hoTen}</span>
                    <button onClick={handleLogout} className="px-5 py-2 rounded border-black border-2">Đăng xuất</button>
                </>
            )
        }else {
            return (
                <>
                <NavLink to = {'/login'}>
                    <button className="px-5 py-2 rounded border-black border-2">Đăng nhập</button>
                </NavLink>
                    <button className="px-5 py-2 rounded border-black border-2">Đăng ký</button>
                </>
            )
        }
    }
    return (
        <div className='space-x-5'>
            {renderContent()}        
        </div>
    )
}

export default UserMenu