import React from 'react'
import { Button, Form, Input, message } from 'antd';
import { useNavigate } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import Lottie from "lottie-react";
import bg_animate from '../../asset/animate_login.json'
import { setLoginActionService } from '../../redux/action/userAction';

const LoginPage = () => {
    let navigate = useNavigate() ; 
    let dispatch = useDispatch() ; 
    // const onFinish = (values) => {
    //     userServ.login(values)
    //     .then((res) => {
    //         console.log(res.data.message);
    //         message.success(res.data.message) ; 
    //         // * gửi dữ liệu lên redux trước khi chuyển trang
    //         // dispatch({
    //         //     type : SET_LOGIN_USER , 
    //         //     payload : res.data.content ,
    //         // })
    //         dispatch(setLoginAction(res.data.content)) ; 
    //         localUserServ.set(res.data.content) ; 
    //         navigate('/') ; 
    //     })
    //     .catch((err) => {
    //         message.error(err.response.data.content);
    //     })
    // };
    const onFinishThunk = (values) => {
        let handleSuccess = () => {
            message.success("Login is successful") ;
            navigate('/') ; 
        }
        dispatch(setLoginActionService(values , handleSuccess)) ;
    }
    const onFinishFailed = (errorInfo) => {
        console.log(errorInfo);
        };
    return (
    <div className="w-screen h-screen p-20 bg-orange-400">
        <div className="container mx-auto p-20 bg-gray-500 rounded-lg flex items-center">
            <div className='w-1/2 h-full'>
            <Lottie animationData={bg_animate} loop={true} />
            </div>
            <div className='w-1/2 h-full'>
                <Form
                name="basic"
                labelCol={{
                    span: 8,
                }}
                wrapperCol={{
                    span: 24,
                }}
                style={{
                    width : '100%',
                }}
                initialValues={{
                    remember: true,
                }}
                onFinish={onFinishThunk}
                onFinishFailed={onFinishFailed}
                autoComplete="off"
                layout='vertical'
                >
                <Form.Item
                    label="Username"
                    name="taiKhoan"
                    rules={[
                    {
                        required: true,
                        message: 'Please input your username!',
                    },
                    ]}
                >
                    <Input />
                </Form.Item>
                <Form.Item
                    label="Password"
                    name="matKhau"
                    rules={[
                    {
                        required: true,
                        message: 'Please input your password!',
                    },
                    ]}
                >
                    <Input.Password />
                </Form.Item>
                <Form.Item
                    wrapperCol={{
                    
                    span: 24,
                    }}
                >
                    <Button type="primary" htmlType="submit" style={{color : 'black'}}>
                    Submit
                    </Button>
                </Form.Item>
                </Form>
            </div>
        </div>
    </div>
);} ; 
export default LoginPage;