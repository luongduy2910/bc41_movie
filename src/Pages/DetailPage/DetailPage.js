import React from 'react'
import { NavLink, useParams } from 'react-router-dom'
import { useEffect , useState } from 'react';
import { movieServ } from '../../service/movieService';
import { Progress } from 'antd';

function DetailPage() {
    const [detailMovie, setDetailMovie] = useState({});
    let params = useParams() ; 
    useEffect(() => {
        let fetchDetail = async () => {
            try {
                let res = await movieServ.getDetailMovie(params.id) ; 
                setDetailMovie(res.data.content)
                
            } catch (error) {
                console.log(error);
            }
        }
        fetchDetail() ; 
    }, [params.id])
    return (
        <div className='container flex'>
            <img className='w-1/4' src={detailMovie.hinhAnh} alt="hinhAnhPhim" />
            <div className="p-5 space-y-5">
                <h2 className='text-4xl font-medium'>{detailMovie.tenPhim}</h2>
                <p className='text-xs text-gray-600'>{detailMovie.moTa}</p>
                <Progress strokeColor={{ '0%': '#108ee9', '100%': '#87d068' }} format={(percent)=> `${percent/10} Điểm`} type="circle" percent={detailMovie.danhGia * 10} />
                <NavLink to = {`/booking/${detailMovie.maPhim}`} className='px-5 py-2 bg-red-500 rounded text-white'>
                    Mua vé
                </NavLink>
            </div>
        </div>
    )
}

export default DetailPage