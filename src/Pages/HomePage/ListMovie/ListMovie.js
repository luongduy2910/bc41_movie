import React from 'react'
import { movieServ } from '../../../service/movieService'
import { useEffect , useState } from 'react';
import ItemMovie from './ItemMovie';

function ListMovie() {
    const [movieList, setMovieList] = useState([]) ; 
    useEffect(() => {
        movieServ.getMovieList()
        .then((res)=> {
            console.log(res.data.content);
            setMovieList(res.data.content) ; 
        })
        .catch((err)=>{
            console.log(err);
        })
    }, []) ; 
    return (  
        <div className='container grid grid-cols-4 gap-10'>
            {movieList.slice(0,8).map((item , index)=> {
                return <ItemMovie data = {item} key={index}/>
            })}
        </div>
    )
}

export default ListMovie