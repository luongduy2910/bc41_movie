import React from 'react'
import { useState , useEffect } from 'react';
import { movieServ } from '../../../service/movieService';
import { Tabs } from 'antd';
import ItemTabsMovie from './ItemTabsMovie';
const onChange = (key) => {
    console.log(key);
};
function TabsMovie() {
    const [danhSachHeThongRap, setDanhSachHeThongRap] = useState([]) ; 
    let renderHeThongRap = () => {
        return danhSachHeThongRap.map((heThongRap) => { 
            return {
                key: heThongRap.maHeThongRap,
                label: <img className='w-16' src ={heThongRap.logo} alt="logo_rap"  /> ,
                children: <Tabs
                                style={{height : 500}}
                                tabPosition='left' 
                                defaultActiveKey="1" 
                                items={heThongRap.lstCumRap.map((cumRap) => {
                                    return {
                                        key: cumRap.tenCumRap,
                                        label: <div className='w-80 truncate'>
                                            <p className='font-medium'>{cumRap.tenCumRap}</p>
                                            <p style={{fontSize : 9}} className='text-xs text-gray-600'>{cumRap.diaChi}</p>
                                        </div>,
                                        children: (
                                            <div style={{height : 500 , overflowY : "scroll" }} className='space-y-5'>
                                            {
                                                cumRap.danhSachPhim.map((phim , index) => { 
                                                    return <ItemTabsMovie key={index} phim={phim}/>; 
                                                })
                                            }
                                        </div>
                                        )
                                    } 
                                })} 
                                onChange={onChange} /> , 
        }
        })
    }
    useEffect(() => {
        movieServ.getMovieByTheater()
        .then((res)=> {
            console.log(res.data.content);
            setDanhSachHeThongRap(res.data.content) ; 
            
        })
        .catch((err)=> {
            console.log(err);
        })
    
    }, [])
    
    return (
        <div className='container py-10 space-y-20'>
            <h2 className='text-red-500 text-2xl font-bold text-center'>Danh Sách Hệ Thống Rạp</h2>
            <Tabs style={{height : 500}} tabPosition='left' defaultActiveKey="1" items={renderHeThongRap()} onChange={onChange} />
        </div>
    )
}

export default TabsMovie