import moment from 'moment/moment';
import React from 'react'

function ItemTabsMovie({phim}) {
    console.log(phim);
    return (
        <div className='flex items-center border-b-2 p-3'>
            <img  src={phim.hinhAnh} className= "w-16 h-24 mr-5" alt="img_phim" />
            <div>
                <h5>{phim.tenPhim}</h5>
                <div className='grid gap-5 grid-cols-2'>
                    {phim.lstLichChieuTheoPhim.slice(0,6).map((item , index) => {
                    return (
                    <span key={index} className="rounded p-1 text-white bg-red-500">{moment(item.ngayChieuGioChieu).format('DD-mm-yyyy ~ hh:ss')}</span>
                    )
                })}
                </div>
            </div>
        </div>
    )
}

export default ItemTabsMovie