import React from 'react'
import ListMovie from './ListMovie/ListMovie'
import TabsMovie from './TabsMovie/TabsMovie'


function HomePage() {
  return (
    <div className='space-y-10'>
      <ListMovie/>
      <TabsMovie/>
    </div>
  )
}

export default HomePage