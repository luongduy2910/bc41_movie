import axios from "axios"
import { BASE_URL, configHeader } from "./config"
// * Tạo một object về dịch vụ người dùng , bên trong là các phương thức liên quan tới user
export const userServ = {
    // * Xây dựng chức năng đăng nhập 
    login : (loginData) => {
        return axios({
            url : `${BASE_URL}/api/QuanLyNguoiDung/DangNhap` , 
            method : 'POST' , 
            data : loginData , 
            headers : configHeader() 
        })
        
    }
}