import {https } from "./config"

export const movieServ = {
    getMovieList : () => {
        // return axios({
        //     url : `${BASE_URL}/api/QuanLyPhim/LayDanhSachPhim?maNhom=GP04` , 
        //     method : "GET" , 
        //     headers : configHeader() , 
            
        // })
        return https.get("api/QuanLyPhim/LayDanhSachPhim?maNhom=GP04")
    } , 
    getMovieByTheater : () => {
        // return axios({
        //     url : `${BASE_URL}/api/QuanLyRap/LayThongTinLichChieuHeThongRap` , 
        //     method : "GET" , 
        //     headers : configHeader() , 
        // })
        return https.get('api/QuanLyRap/LayThongTinLichChieuHeThongRap')
    } , 
    getDetailMovie : (id) => {
        return https.get(`/api/QuanLyPhim/LayThongTinPhim?MaPhim=${id}`) ; 
    }
}

// kiến thức axios instance 

