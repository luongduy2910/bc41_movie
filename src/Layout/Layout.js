import React from 'react'
import Header from '../Components/Header/Header'
import Footer from '../Components/Footer/Footer'

function Layout({Component}) {
  return (
    <div className='space-y-20 h-full min-h-screen'>
        <Header/>
        <Component/>
        <Footer/>
    </div>
  )
}

export default Layout