import { userServ } from "../../service/userService"
import { SET_LOGIN_USER } from "../constant/userConstant"
import { localUserServ } from "../../service/localService";
export const setLoginAction =  (values) => {
    return {
        type : SET_LOGIN_USER , 
        payload : values ,
    }
}
export const setLoginActionService = (values , onSuccess) => {
    return (dispatch) => { 
        userServ.login(values)
        .then((res) => {
            console.log(res);
            dispatch({
                type : SET_LOGIN_USER , 
                payload : res.data.content  ,
            }) 
            localUserServ.set(res.data.content) ;
            onSuccess() ; 
        }).catch((err) => {
            console.log(err);
        });
    }
}