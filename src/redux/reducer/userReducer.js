import { localUserServ } from "../../service/localService";
import { SET_LOGIN_USER } from "../constant/userConstant";

const initialState = {
    // * Nơi chứa thông tin user
    userInfo : localUserServ.get() , 
}

let userReducer = (state = initialState, { type, payload }) => {
    switch (type) {
        case SET_LOGIN_USER : {
            return {...state , userInfo : payload} ; 
        }
    default:
        return {...state} ;
    }
}
export default userReducer
